#KROME is a nice and friendly chemistry package for a wide range of 
# astrophysical simulations. Given a chemical network (in CSV format) 
# it automatically generates all the routines needed to solve the kinetic 
# of the system, modelled as system of coupled Ordinary Differential 
# Equations. 
# It provides different options which make it unique and very flexible. 
# Any suggestions and comments are welcomed. KROME is an open-source 
# package, GNU-licensed, and any improvements provided by 
# the users is well accepted. See disclaimer below and GNU License 
# in gpl-3.0.txt.
#
# more details in http://kromepackage.org/
# also see https://bitbucket.org/krome/krome_stable
#
# Written and developed by Tommaso Grassi
# tommasograssi@gmail.com,
# Starplan Center, Copenhagen.
# Niels Bohr Institute, Copenhagen.
#
# Co-developer Stefano Bovino
# sbovino@astro.physik.uni-goettingen.de
# Institut fuer Astrophysik, Goettingen.
#
# Others (alphabetically): D.Galli, F.A. Gianturco, T. Haugboelle,
# J.Prieto, D.R.G. Schleicher, D. Seifried, E. Simoncini,
# E.Tognelli
#
#
# KROME is provided "as it is", without any warranty. 
# The Authors assume no liability for any damages of any kind 
# (direct or indirect damages, contractual or non-contractual 
# damages, pecuniary or non-pecuniary damages), directly or 
# indirectly derived or arising from the correct or incorrect 
# usage of KROME, in any possible environment, or arising from 
# the impossibility to use, fully or partially, the software, 
# or any bug or malefunction.
# Such exclusion of liability expressly includes any damages 
# including the loss of data of any kind (including personal data)

# THIS FILE CONTAINS FUNCTIONS AND CLASSES FOR THE MAIN 
# KROME PYTHON SCRIPT


import sys
##################################
class molec():
	name = "" #molecule name
	charge = 0 #charge (0=neutral)
	mass = 0. #mass in g
	zatom = 0 #atomic number (also for molecules)
	neutrons = 0 #total number of neutrons
	ename = "" #exploded name (e.g. H2C4=CCCCHH)
	fname = "" #f90 name (e.g. H+=Hj, D-=Dk)
	phname = "" #compact name for photoionizations (e.g. Fe++++=Fej4)
	coolname = "" #name for cooling, e.g. CIV
	fidx = "idx_" #index for fortran (e.g. H+=idx_Hj, D-=idx_Dk)
	is_atom = False #flag to identify atoms
	chempot = 0. #chemical potential (J/mol)
	poly1 = [0.e0]*7 #nasa polynomials (usually 200-1000K)
	poly2 = [0.e0]*7 #nasa polynomials (usually 1000-5000K)
	Tpoly = [0.e0]*3 #temperature limits
	idx = 0 #species index
	enthalpy = 0.e0 #enthalpy of formation
	atomcount = dict() #dictionary containin the count of atoms including zero (e.g H2O is {"H":2, "O":1, "C":0, ...})
	atomcount2 = dict() #dictionary containin the count of atoms without zero species (e.g H2O is {"H":2, "O":1})
	natoms = 0 #the number of atoms (e.g. diatomic=2)
	ve_vib = "__NONE__" #vibrational constant in K
	be_rot = "__NONE__" #rotational constant in K

	def __init__(self):
		self.poly1 = [0.e0]*7
		self.poly2 = [0.e0]*7
		self.Tpoly = [0.e0]*3
		self.atomcount = dict()	
##################################
class reaction():
	verbatim = "" #reaction written as string (e.g. A+B->C+D)
	reactants = [] #list of reactants (molec objects)
	products = [] #list of products (molec objects)
	Tmin = "0.d0" #min value of temperature range
	Tmax = "0.d0" #max value of temperature range
	TminOp = "" #min operator
	TmaxOp = "" #max operator
	pseudo_hash = "" #pseudo hash
	krate = "" #reaction rate written in F90 style
	idx = 0 #reaction index
	idxph = 0 #reaction index photorate (start from 1, 0=not a photorate)
	RHS = "" #ODE RHS in F90 style k(1)*n(2)*n(3)
	RHSvar = "" #ODE RHS as variable k1n2n3
	kphrate = None #photochemical rate
	dH = None #enthalpy of formation
	qeff = 0.e0 #effective Q value (for nuclear reactions)
	curlyR = [] #reactants curlyness
	curlyP = [] #products curlyness
	nuclearMult = "" #nuclear multeplicty factor 1/(n!)
	hasTlimitMax = hasTlimitMin = True #flag to determine the presence of Temperature limits
	group = "__DEFAULT__"
	canUseTabs = True #flag if this reaction can use tabs
	ifrate = "" #if condition on rate, e.g. if(Tgas>1d2):
	isCR = False #flag this reaction as CR
	isXRay = False #flag this reaction as XRay
	#method: constructor to initialize lists
	def __init__(self):
		self.reactants = []
		self.products = []

	#method: build verbatim from reactants and products
	def build_verbatim(self):
		myr = []
		myp = []
		for r in self.reactants:
			if(r.name!="dummy"):
				myr.append(r.name)	
		for p in self.products:
			if(p.name!="dummy"):
				myp.append(p.name)
		self.verbatim = " + ".join(myr)+" -> "+" + ".join(myp)

	#method: build photochemical rate
	def build_phrate(self,photoBlock=False):
		if(not("krome_kph_auto" in self.krate) and not(photoBlock)): return
		myr = self.reactants
		self.kphrate = self.krate.replace("krome_kph_auto=","")
		self.krate = "photoBinRates("+str(self.idxph)+")"
		#if(self.krate.strip()=="krome_kph_auto"): 
		#	self.krate = "krome_kph_"+myr[0].phname
		#else: 
		#	self.krate = "krome_kph_" + myr[0].phname.capitalize() + "R" + str(self.idx)

	#method: build RHS
	def build_RHS(self,useNuclearMult=False):
		if(self.idx<=0):
			print "************************************************************"
			print "ERROR: reaction index (reaction.idx) must be greater than 0"
			print "Probably you have to define idx"
			print "************************************************************"
			sys.exit()

		#keeps into account nuclear multeplicity (if option is enabled)
		nuclearMult = "" #default nuclear multeplicity value
		if(useNuclearMult):
			uncurledR = [self.reactants[i].name for i in range(len(self.reactants)) if not(self.curlyR[i])]
			if(len(uncurledR)==2):
				if(uncurledR[0]==uncurledR[1]): nuclearMult = "0.5d0*"
			if(len(uncurledR)==3):
				if(uncurledR[0]==uncurledR[1] and uncurledR[1]==uncurledR[2]): nuclearMult = "0.16666666667d0*"
				if(uncurledR[0]==uncurledR[1] and uncurledR[1]!=uncurledR[2]): nuclearMult = "0.5d0*"
				if(uncurledR[1]==uncurledR[2] and uncurledR[0]!=uncurledR[1]): nuclearMult = "0.5d0*"
		self.nuclearMult = nuclearMult
		self.RHS = nuclearMult+"k("+str(self.idx)+")"
		self.RHSvar = "kflux"+str(self.idx)
		ns = []
		i = 0
		for r in self.reactants:
			if(self.curlyR[i]): continue #skip curly reactants
			i += 1
			ns.append("n("+str(r.fidx)+")")
			if(r.idx<=0):
				print "************************************************************"
				print "ERROR: species index (molec.idx) must be greater than 0"
				print "Probably you have to define idx"
				print "************************************************************"
				sys.exit()
		if(len(ns)>0): self.RHS += "*" + ("*".join(ns))

	#method:build pseudo hash (for unique reactions to avoid duplicates)
	def build_pseudo_hash(self):
		rname = []
		pname = []
		for r in self.reactants:
			rname.append(r.name)
		
		for p in self.products:
			pname.append(p.name)
		self.pseudo_hash = ("_".join(sorted(rname)))+"|"+("_".join(sorted(pname))) 

	#method: check reaction (mass and charge conservation)
	def check(self,mode="ALL"):
		mass_reactants = mass_products = 0.e0
		charge_reactants = charge_products = 0
		for r in self.reactants:
			mass_reactants += r.mass
			charge_reactants += r.charge
		
		for p in self.products:
			mass_products += p.mass
			charge_products += p.charge
		if(mass_reactants!=0 and (mode=="ALL" or "MASS" in mode)):
			if(abs(1.e0-mass_products/mass_reactants)>1e-6):
				print "************************************************"
				print "WARNING: problem with mass conservation in reaction", self.idx
				print "reaction:",self.verbatim
				print "reactants:", [r.name for r in self.reactants], mass_reactants
				print "products:", [p.name for p in self.products], mass_products
				print "mass ratio (prods/reacts):",mass_products/mass_reactants, "(should be 1.0)"
				print "You can remove this check with the -nomassCheck option"	
				print "************************************************"
				a = raw_input("Any key to continue q to quit... ")
				if(a=="q"): print sys.exit()
		if(abs(charge_products - charge_reactants)!=0 and (mode=="ALL" or "CHARGE" in mode)):
			print "************************************************"
			print "WARNING: problem with charge conservation in reaction", self.idx
			print "reaction:",self.verbatim
			print "reactants:", [r.name for r in self.reactants], charge_reactants
			print "products:", [p.name for p in self.products], charge_products
			print "You can remove this check with the -nochargeCheck option"
			print "************************************************"
			a = raw_input("Any key to continue q to quit... ")
			if(a=="q"): print sys.exit()

	#calcluate enthalpy of formation
	def enthalpy(self):
		if("krome_kph" in self.krate): return

		if(len(self.reactants)==0 and len(self.products)==0):
			self.dH = 0e0
			return
		if(len(self.reactants)==0 or len(self.products)==0):
			print "ERROR: you have called enthalpy calculation"
			print " with empty reactants and/or products!"
			sys.exit()
		reag = self.reactants #copy reactants
		prod = self.products #copy products
		available = True #flag for species availablity in the enthalpy dictionary
		rH = pH = 0.e0 #init reactants and prodcuts enthalpy eV
		#loop on reactants
		for xr in reag:
			xr.name = xr.name.upper()
			#print xr.name
			#if(not(xr.name in deltaH)):
			#	available = False
			#	break
			rH += xr.enthalpy #deltaH[xr.name] 

		#loop on products
		for xp in prod:
			xp.name = xp.name.upper()
			#print xp.name
			#if(not(xp.name in deltaH)):
			#	available = False
			#	break	
			pH += xp.enthalpy
		self.dH = None
		if(available): self.dH = (rH-pH)*1.60217657e-12 #eV->erg (cooling<0)


	#calculate reverse reaction using polynomials
	def doReverse(self):
		pidx = "(/"+(",".join([x.fidx for x in self.products]))+"/)"
		ridx = "(/"+(",".join([x.fidx for x in self.reactants]))+"/)"
		ndif = len(self.reactants)-len(self.products)
		kk = "("+self.krate+") / exp(revKc(Tgas,"+ridx+","+pidx+"))"
		if(ndif!=0): kk ="0.d0"  #" * (1.3806488d-2 * Tgas)**("+str(ndif)+")"
		return kk

#################################
#create tabvar (probably not the best interface ever)
def create_tabvar(mytabvar,mytabpath,mytabxxyy,anytabvars,anytabfiles,anytabpaths,anytabsizes,coevars):
	if(mytabvar.split("_")[0].lower()!="user"):
		print "ERROR: to avoid conflicts common variables with @tabvar should begin with user_"
		print " you provided: "+mytabvar
		print " it should be: user_"+mytabvar
		sys.exit()
	#check if file exists
	if(not(file_exists(mytabpath))):
		print "ERROR: file "+mytabpath+" not found!"
		print " note that the path must be relative to the ./krome command"
		sys.exit()

	#read the size of the table from the first line file
	fhtab = open(mytabpath,"rb")
	for tabrow in fhtab:
		stabrow = tabrow.strip()
		if(stabrow==""): continue
		if(stabrow[0]=="#"): continue		
		if("," in stabrow): 
			mytabsize = [xx.strip() for xx in stabrow.split(",")]
		else:
			print "ERROR: the file "+mytabpath+" must contain the size of the"
			print " table in the first line (comma separated, e.g. 50,30)"
			sys.exit()
		break
	fhtab.close()

	#retrieve filename from the path
	mytabfile = mytabpath.split("/")[-1] #read the last value

	#store the data in the global arrays
	anytabvars.append(mytabvar)
	anytabfiles.append(mytabfile)
	anytabpaths.append(mytabpath)
	anytabsizes.append(mytabsize)


	anytabx = mytabvar+"_anytabx(:)"
	anytaby = mytabvar+"_anytaby(:)"
	anytabz = mytabvar+"_anytabz(:,:)"
	anytabxmul = mytabvar+"_anytabxmul"
	anytabymul = mytabvar+"_anytabymul"
	tabf =  "fit_anytab2D("+anytabx+", &\n"+anytaby+", &\n"+anytabz+", &\n"+anytabxmul+", &\n"+anytabymul+", &\n"+mytabxxyy+")"
	if(not(mytabvar in coevars)):
		coevars[mytabvar] = [len(coevars),tabf]

	print "Found tabvar:",mytabvar,"("+mytabpath+")", "["+(",".join(mytabsize))+"]"

#############################
def addVarCoe(mytabvar,tabf,coevars):
	if(not(mytabvar in coevars)):
		coevars[mytabvar] = [len(coevars),tabf]

#############################
#cooling index list
def get_cooling_index_list():
	#the keys of this list must be lowercase. 
	#the number is the corresponding integer index for the given cooling 
	idxcoo = {"H2":1,"H2GP":2,"atomic":3, "CEN":3, "HD":4, "Z":5, "metal":5, "dH":6, "enthalpic":6, "dust":7,\
		"compton":8,"CIE":9, "continuum":10, "cont":10,"exp":11,"expansion":11,"ff":12,"bss":12,"custom":13}

	#loop on the index to write variables as idx_cool_H2 = 1
	idxscoo = []
	maxv = 0 #maximum index found is the size of the cooling array
	for (k,v) in idxcoo.iteritems():
		idxscoo.append([v,"idx_cool_"+k+" = "+str(v)])
		maxv = max(maxv,v)
	idxscoo = sorted(idxscoo,key=lambda x:x[0])
	idxscoo.append([99,"ncools = "+str(maxv)])
	return [x[1] for x in idxscoo]

#############################
#heating index list
def get_heating_index_list():
	idxhea = {"chem":1,"compress":2, "compr":2, "photo":3, "dH":4, "enthalpic":4, "photoAv":5, "Av":5,\
		"CR":6, "dust":7, "xray":8}

	idxshea = []
	maxv = 0
	for (k,v) in idxhea.iteritems():
		idxshea.append([v, "idx_heat_"+k+" = "+str(v)])
		maxv = max(maxv,v)
	idxshea = sorted(idxshea,key=lambda x:x[0])
	idxshea.append([99,"nheats = "+str(maxv)])
	return [x[1] for x in idxshea]


####################################
#solar metallicities
def get_solar_abundances():
	#solar abundances from Tab.1 in Asplund+2009
	# following their definition
	#  log10(epsilon) = log10(n/nH) + 12
	# where n is the number densiity of the given element, 
	# while nH is the number density of H
	solar_abs = {
		"D":12.00e0,
		"He":10.93e0,
		"Li":1.05e0,
		"Be":1.38e0,
		"B":2.70e0,
		"C":8.43e0,
		"N":7.83e0,
		"O":8.69e0,
		"F":4.56e0,
		"Ne":7.93e0,
		"Na":6.24e0,
		"Mg":7.60e0,
		"Al":6.45e0,
		"Si":7.51e0,
		"P":5.41e0,
		"S":7.12e0,
		"Cl":5.50e0,
		"Ar":6.40e0,
		"K":5.03e0,
		"Ca":6.34e0,
		"Ti":4.95e0,
		"Mn":5.43e0,
		"Fe":7.50e0,
		"Ni":6.22e0
		}

	solar_out = dict()
	for k,v in solar_abs.iteritems():
		solar_out[k] = str(1e1**(v-12e0))

	return solar_out



###################################
#vibrational constant dictionary
#IRIKURA J. Phys. Chem. Ref. Data, Vol. 36, No. 2, 2007
#energy in cm-1, returns K
#returns False if arg not found in list
def get_ve_vib(arg):
	ve = {"H2":4401.213,
		"HD":3813.15,
		"D2":3115.5,
		"C2":1855.0663,
		"C2-":1781.189,
		"CH":2860.7508,
		"CO":2169.75589,
		"CO+":2214.127,
		"N2":2358.57,
		"N2+":2207.0115,
		"NH":3282.72,
		"NO":1904.1346,
		"NO+":2376.72,
		"O2":1580.161,
		"O2+":1905.892,
		"OH":3737.761}
	if(arg in ve):
		return ve[arg]*1.42879e0 #cm-1 to K 
	else:
		False

###################################
#rotational constant Be dictionary
#from NIST and Atkins Book
#constant in cm-1, returns K
#returns False if arg not found in list
def get_be_rot(arg):
	ve = {"H2":60.853,
		"H2+":42.9,
		"HD":45.644,
		"D2":30.443,
		"N2":1.9982,
		"O2":1.4264,
		"CO":2.78}
	if(arg in ve):
		return ve[arg]*1.42879e0 #cm-1 to K 
	else:
		False

##################################
#check if a file exists
def file_exists(fname):
	import os
	return os.path.exists(fname)

####################################
def int_to_roman(input):
	#Convert an integer to Roman numerals.
	#from http://code.activestate.com/recipes/81611-roman-numerals/
	if type(input) != type(1):
		print "ERROR: expected integer, got " + type(input)
		sys.exit()
	if not 0 < input < 4000:
		print "ERROR: Argument must be between 1 and 3999"
		sys.exit()
	ints = (1000, 900,  500, 400, 100,  90, 50,  40, 10,  9,   5,  4,   1)
	nums = ('M',  'CM', 'D', 'CD','C', 'XC','L','XL','X','IX','V','IV','I')
	result = ""
	for i in range(len(ints)):
		count = int(input / ints[i])
		result += nums[i] * count
		input -= ints[i] * count
	return result

##################################
#store file in a list
def store_file(fname):
	fh = open(fname,"rb")
	fle = []
	for x in fh:
		fle.append(x)
	fh.close()
	return fle

##################################
#restore file from a list
def restore_file(fname,fle):
	fout = open(fname,"w")
	for x in fle:
		fout.write(x)
	fout.close()

##################################
def get_terminal_size(fd=1):
    """
    Returns height and width of current terminal. First tries to get
    size via termios.TIOCGWINSZ, then from environment. Defaults to 25
    lines x 80 columns if both methods fail.
 
    :param fd: file descriptor (default: 1=stdout) 
	from bit.ly/HteEcQ
    """
    try:
        import fcntl, termios, struct
        hw = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ, '1234'))
    except:
        try:
            hw = (os.environ['LINES'], os.environ['COLUMNS'])
        except:  
            hw = (25, 80)
 
    return hw

##################################
#return an example for test.f90
def get_example(nsp,useX):
	sfile = """
		!###################################################
		! WARNING:This is a test auto-generated by KROME, in order to
		! show a bare-minimal code to call the KROME's subroutine.
		! Most of the values could not be appropriate for your 
		! problem, since this test is only intended as a general	
		! purpose example.

		program test
			use krome_main !use krome (mandatory)
			use krome_user !use utility (for krome_idx_* constants and others)
			implicit none
			integer,parameter::nsp=krome_nmols !number of species (common)
			real*8::Tgas,dt,x(nsp)@rho@,spy

			spy = 3.65d2 * 2.4d1 * 3.6d3 !seconds per year

			call krome_init() !init krome (mandatory)

			x(:) = 1d-20 !default abundances
			x(krome_idx_H) = @nH@ !hydrogen initial abundance
			@norm@
		
			Tgas = 1d3 !gas temperature (K)
			dt = 1d6 * spy !time-step (s)
			@rho_init@

			!call the solver
			call krome(x(:),@rhof@ Tgas, dt) !call KROME

			print *,"Test OK!"
		
		end program test

	"""
	if(useX):
		sfile = sfile.replace("@rho@",",rho").replace("@nH@","1.d0").replace("@norm@","x(:) = x(:) / sum(x) !normalize")
		sfile = sfile.replace("@rho_init@","rho = 1d-18 !gas density (g/cm3)").replace("@rhof@"," rho,")
	else:
		sfile = sfile.replace("@rho@","").replace("@nH@","1.d4").replace("@norm@","")
		sfile = sfile.replace("@rho_init@","").replace("@rhof@","")
	sfile = sfile.replace("@nsp@",str(nsp))

	if("@" in sfile):
		print sfile
		print "ERROR: missing replacement in get_example() function!"
		sys.exit()
	return sfile


#########################################
#parse for variables in a f90 expression
def parsevar(arg):
	arg = arg.lower()
	tks = ["+","-","*","/","(",")"]
	for tk in tks:
		arg = arg.replace(tk,"@")
	while "@@" in arg:
		arg = arg.replace("@@","@")
	#tks2 = ["exp","sqrt","log","log10"]
	return arg.split("@")

##################################
#extend the list slist with the temperature shortcuts 
# for the reaction rea
def get_Tshortcut(rea,slist,cvars=[]):
	shcut = ["logT = log10(Tgas) !log10 of Tgas (#)",
	"lnT = log(Tgas) !ln of Tgas (#)",
	"Te = Tgas*8.617343d-5 !Tgas in eV (eV)",
	"lnTe = log(Te) !ln of Te (#)",
	"T32 = Tgas*0.0033333333333333335 !Tgas/(300 K) (#)",
	"t3 = T32 !alias for T32 (#)",
	"t4 = Tgas*1d-4 !Tgas/1d4 (#)",
	"invT = 1.d0/Tgas !inverse of T (1/K)",
	"invT32 = 1.d0/T32 !inverse of T32 (1/K)",
	"invTgas = 1.d0/Tgas !inverse of T (1/K)",
	"invTe = 1.d0/Te !inverse of T (1/eV)",
	"sqrTgas = sqrt(Tgas) !Tgas rootsquare (K**0.5)",
	"invsqrT32 = 1.d0/sqrt(T32)",
	"invsqrT = 1.d0/sqrTgas",
	"sqrT32 = sqrt(T32)",
	"Tgas2 = Tgas*Tgas",
	"Tgas3 = Tgas2*Tgas",
	"Tgas4 = Tgas3*Tgas",
	"T0 = 288d0 !standard temperature (K)",
	"T02 = T0*T0",
	"T03 = T02*T0",
	"T04 = T03*T0",
	"T0inv = 1.d0/T0"]

	#split the first part of the shortcut and uses it as a key (e.g. t = Tgas -> t)
	sckey = [(x.split("="))[0].strip().lower() for x in shcut]
	shcut = sorted(shcut, key=lambda x:len(x.split("=")[0].strip()), reverse=True)

	#loop on the shortcuts to find if the rate coefficient employs them
	krea = rea.krate
	for x in shcut:
		ax = x.split("=") #split the shortcut
		xvar = parsevar(krea) #parse the variable in the rate coefficient
		krea = krea.replace(ax[0].strip(),"")
		if((ax[0].strip().lower() in xvar) and not(x in slist)):
			#search for dependencies between shortcuts
			xtmp = x
			for xx in shcut:
				axx = xx.split("=") #split the shortcut
				xxvar = parsevar(xtmp)  #parse the variables in the rate coeffcient found
				xtmp = xtmp.replace(axx[0].strip(),"")
				if((axx[0].strip().lower() in xxvar) and not(xx in slist)):
					slist.append(xx) #append the dependent shortcut
			slist.append(x) #append the main shortcut

	#sort the shortcuts by using the index in the list sckey to follow the hierarchy
	slist = sorted(slist, key=lambda x:sckey.index((x.split("="))[0].strip().lower()))

	#remove shortcuts that are already use as user-defined variables
	slistu = []
	for x in slist:
		xkey = x.split("=")[0] #get the variable name
		xFound = False
		#loop on user-defined variables
		for cv in cvars:
			#when variable is found skip it
			if(xkey.lower().strip()==cv.lower().strip()): 
				xFound = True
				break
		if(not(xFound)):
			slistu.append(x)

	#keep only unique shortcuts (remove duplicates)
	ulist = []
	for x in slistu:
		if(not(x in ulist)): ulist.append(x)

	return ulist

##################################
#get list of available commands
def get_usage():
	print "use -h to see the help"
	sys.exit()

##################################
#truncate F90 expression using sep as separator for blocks shorter than sublen
def truncF90(mystr, sublen, sep):
	#split (&\n) the string mystr in parts smaller tha sublen using sep as separator
	if(mystr.strip()==""): return mystr
	mystr = mystr.replace("**","##")
	astr = mystr.split(sep)
	s = z = ""
	first = True
	for x in astr:
		if(len(z+x)>sublen and not(first)):
			s += "&\n"
			z = ""
		zep = sep
		if(first): zep = "" 
		s += zep + x
		z += zep + x
		first = False
	return s.replace("##","**")	

################################
def at_extract(arg):
	aarg = arg.split(":")
	if(len(aarg)!=2):
		print "ERROR: @label:value format not respected for"
		print arg
		sys.exit()
	aarg[0] = aarg[0].replace("@","")
	return {aarg[0]:aarg[1]}

##################################
#double format for f90 expressions
def format_double(snum):
	snum = str(snum)
	#format string number to F90 double
	if("d" in snum): return snum
	if("e" in snum): return snum.replace("e","d")	
	return snum+"d0"

##################################
#format the subelement according to its size (Fe, N, ...)
def format_subel(subel):	
	if(len(subel)==1):
		fsubel = subel.upper()
		if(fsubel=="G"): fsubel = "g"
	elif(len(subel)==2):
		fsubel = subel[0].upper() + subel[1].lower()
	else:
		fsubel = subel
	return fsubel

##################################
#check if the value s is a number or not (return logical)
def is_number(s):	
	try:
		float(s)
		return True
	except ValueError:
		return False

##################################
#parse molecule name using dictionary and atoms list
def parser(name, mass_dic, atoms, thermo_data):

	mymol = molec() #oggetto molec
	namecp = name.upper()
	if(namecp=="E-"): namecp = "E" #avoid double negative charge
	ename = [] #exploded name
	mass = 0. #init mass
	is_atom = True #atom flag
	founds = 0 #atoms found
	
	#if you change these check the same values in kromeobj
	#(employed here for computing number of neutrons)
	me = 9.10938188e-28 #electron mass (g)
	mp = 1.67262158e-24 #proton mass (g)
	mn = 1.6725e-24 #neutron mass (g)


	#atomic number dictionary (add here atoms if needed)
	zdic = {"E":0,
		"D":1,
		"H":1,
		"HE":2,
		"LI":3,
		"BE":4,
		"B":5,
		"C":6,
		"N":7,
		"O":8,
		"F":9,
		"NE":10,
		"NA":11,
		"MG":12,
		"AL":13,
		"SI":14,
		"P":15,
		"S":16,
		"CL":17,
		"AR":18,
		"K":19,
		"TI":22,
		"CA":20,
		"CR":24,
		"MN":25,
		"FE":26,
		"NI":28}


	#add isotopes Z numbers (same as non-isotopes)
	zdic_copy = zdic.copy()
	for k,v in zdic_copy.iteritems():
		for i in range(2,56):
			zdic[str(i)+k] = v

	#check for fake species
	if("FK" in name):
		mymol.name = name #name
		mymol.mass = 0e0 #mass (g)
		mymol.ename = name #exploded name
		mymol.charge = 0 #charge
		mymol.zatom = 0 #atomic number
		mymol.fname = name #f90 name
		mymol.is_atom = True #atom flag
		mymol.fidx = "idx_"+name #f90 index
		mymol.neutrons = 0 #number of neutrons
		return mymol
	
	zatom = 0 #atomic number init
	#loop over charcters
	for atm in atoms:
		a = atm.upper() #capitalize name
		if(not(a) in namecp): continue #skip
		#loop to up to _30 subscript
		for j in range(30):
			if(a in namecp):
				idx = namecp.find(a) #find position
				subs = a
				mult = "0" #multiplicator
				for i in range(idx+len(a),len(namecp)):
					if(not(is_number(namecp[i]))): break
					mult += namecp[i] #find multiplicator
					subs += namecp[i]
				imult = max(int(mult),1) #evaluate multiplicator (must be >0)
				mass += mass_dic[a]*imult #compute mass
				if(a in zdic): zatom += zdic[a]*imult #increase atomic number
				if(format_subel(a) in mymol.atomcount): 
					mymol.atomcount[format_subel(a)] += imult #increase atom count
				else:
					mymol.atomcount[format_subel(a)] = imult #init atom count
				ename += [format_subel(a)]*imult #exploded name
				namecp = namecp.replace(subs,"",1) #remove found in name
				if(namecp==""): break #if nothing more to find break loop
		if(a!="+" and a!="-"): founds += imult #count found atoms for is_atom
	if(founds>1): is_atom = False #atoms have only one atom (viz.)
	
	mymol.atomcount2 = dict()
	natoms = 0
	for (k,v) in mymol.atomcount.iteritems():
		if(v>0):
			mymol.atomcount2[k] = v
			if(k!="+" and k!="-"): natoms += v

	#print name,mymol.atomcount2

	mymol.natoms = natoms #number of atoms (e.g. diatom=2)

	#get vibrational constant in K
	if(get_ve_vib(name)):
		mymol.ve_vib = get_ve_vib(name)
	#get rotational constant in K
	if(get_be_rot(name)):
		mymol.be_rot = get_be_rot(name)

	mymol.name = name #name
	mymol.mass = mass #mass (g)
	mymol.ename = sorted(ename) #exploded name
	mymol.charge = 0 #charge
	mymol.zatom = zatom #atomic number
	mymol.fname = name.replace("+","j").replace("-","k") #f90 name
	#cooling name is only for atoms, e.g. CIV
	mymol.coolname = name
	if(is_atom):
		mymol.coolname = getRomanName(name)

	mymol.is_atom = is_atom #atom flag
	f90idx = "idx_"+name.replace("+","j").replace("-","k").replace("(","_").replace(")","").replace("[","").replace("]","_") #f90 index
	if(f90idx.endswith("_")): f90idx = f90idx[:-1] #remove last underscore if any
	mymol.fidx = f90idx #index in f90 format
	
	if("+" in name): mymol.charge = name.count("+") #get + charge
	if("-" in name): mymol.charge = -name.count("-") #get - charge

	#number of neutrons (computed using total mass)
	Nn = round((mymol.mass - (me*(mymol.zatom - mymol.charge) + mp*(mymol.zatom))) / mn,0)
	mymol.neutrons = int(Nn)


	#name for photoionization reactions (e.g. Sijjj = Sij3)
	jj = kk = ""
	if(mymol.charge==1): jj = "j"
	if(mymol.charge>1): jj = "j"+str(mymol.charge)
	if(mymol.charge==-1): kk = "k"
	if(mymol.charge<-1): kk = "k"+str(mymol.charge)
	mymol.phname = name.replace("+","").replace("-","") + jj + kk

	#electron has negative charge
	if(mymol.name=="E"): mymol.charge = -1
	
	#thermal data
	if(mymol.name in thermo_data):
		mymol.poly1 = thermo_data[mymol.name][10:] #NASA polynomials lower T interval (min-med)
		mymol.poly2 = thermo_data[mymol.name][3:10] #NASA polynomials upper T interval (med-max)
		mymol.Tpoly = thermo_data[mymol.name][0:3] #(K) [min,med,max] T interval limits

	#compute enthaly @300K using NASA poly
	if(mymol.Tpoly[1]<3e2):
		p = mymol.poly1 #copy polynomials in the lower range
	else:
		p = mymol.poly2 #copy poly in the upper range
	Tgas = 300. #K
	polyH = p[0] + p[1]*0.5*Tgas + p[2]*Tgas**2/3. + p[3]*Tgas**3*0.25 + p[4]*Tgas**4*0.2 + p[5]/Tgas
	mymol.enthalpy = polyH*8.314472e-3*Tgas*0.01036410e0 #eV

	#checks parsing results
	if(len(namecp)>0): 
		print "************************************************"
		print "ERROR: Parsing problem for", name
		print "Unknown subelements in substring \"" + namecp +"\"."
		print "Probably you have to add some subelements to the dictionary mass_dic."
		if(len(atoms)<30):
			print "Dictionary now contains the following subelements:"
			print atoms
		print "************************************************"
		sys.exit()
	return mymol

###################################
#convert the name of a species to roman name, e.g. C++ to CIII
# also for anion: C- to CmI
def getRomanName(argmetal):
	#cation
	if("+" in argmetal):
		mname = argmetal.replace("+","") + int_to_roman(argmetal.count("+")+1)
	#anion
	elif("-" in argmetal):
		mname = argmetal.replace("-","") + "m"+int_to_roman(argmetal.count("-")+1)
	#neutral
	else:
		mname = argmetal+"I"
	return mname


######################################
#return a list of the core files of krome to check the consistency
def get_file_list():
	files = []
	files.append("krome")
	files.append("kromeobj.py")
	files.append("kromelib.py")
	files.append("patches")
	files.append("tests")
	files.append("tests/test.f90")
	files.append("tests/Makefile")
	files.append("tests/MakefileF90")
	files.append("build")
	files.append("tools")
	files.append("src")
	files.append("src/krome_constants.f90")
	files.append("src/krome_tabs.f90")
	files.append("src/kromeF90.f90")
	files.append("src/krome_user.f90")
	files.append("src/krome_stars.f90")
	files.append("src/krome_user_commons.f90")
	files.append("src/krome_subs.f90")
	files.append("src/krome_dust.f90")
	files.append("src/krome_cooling.f90")
	files.append("src/krome_photo.f90")
	files.append("src/krome_ode.f90")
	files.append("src/krome_reduction.f90")
	files.append("src/krome.f90")
	files.append("src/krome_heating.f90")
	files.append("src/krome_commons.f90")
	files.append("data")
	files.append("data/crossSect.dat")
	files.append("data/heatxH.dat")
	files.append("data/optSi.dat")
	files.append("data/ip.dat")
	files.append("data/optC.dat")
	files.append("data/ratexHe.dat")
	files.append("data/escape_H2.dat")
	files.append("data/heatxHe.dat")
	files.append("data/coolO2.dat")
	files.append("data/thermo30.dat")
	files.append("data/coolZ.dat")
	files.append("data/ratexH.dat")
	files.append("data/database")
	files.append("data/database/photoioniziation.dat")
	files.append("data/database/collisional_ionization.dat")
	files.append("data/database/radiative_rec_lowT.dat")
	files.append("data/database/radiative_rec.dat")
	files.append("solver")
	files.append("solver/nleq_all.f")
	files.append("solver/opkda2.f")
	files.append("solver/opkdmain.f")
	files.append("solver/dvode_f90_license.txt")
	files.append("solver/opkda1.f")
	files.append("solver/dvode_f90_m.f90")
	files.append("networks")
	return files

###################################
# modified from:
# http://akiscode.com/articles/sha-1directoryhash.shtml
# Copyright (c) 2009 Stephen Akiki
# MIT License (Means you can do whatever you want with this)
#  See http://www.opensource.org/licenses/mit-license.php

def GetHashofDirs():
	import hashlib, os
	SHAhash = hashlib.sha1()
	fles = get_file_list() 
	for fle in fles:
		try:
			f1 = open(fle, 'rb')
		except:
			continue
		buf = f1.read(4096)
		SHAhash.update(hashlib.sha1(buf).hexdigest())

	return SHAhash.hexdigest()

##################################
def clear_dir(folder):
	import os
	for the_file in os.listdir(folder):
	    file_path = os.path.join(folder, the_file)
	    try:
		if os.path.isfile(file_path):
		    os.unlink(file_path)
	    except Exception, e:
		print e


#################################
# returns the licence of KROME
def get_licence_header(version, codename, short=False):
	import datetime
	header =  """!!*************************************************************
	!! This file has been generated with:
	!! krome #version# "#codename#" on #date#.
	!!
	!!KROME is a nice and friendly chemistry package for a wide range of 
	!! astrophysical simulations. Given a chemical network (in CSV format) 
	!! it automatically generates all the routines needed to solve the kinetic 
	!! of the system, modelled as system of coupled Ordinary Differential 
	!! Equations. 
	!! It provides different options which make it unique and very flexible. 
	!! Any suggestions and comments are welcomed. KROME is an open-source 
	!! package, GNU-licensed, and any improvements provided by 
	!! the users is well accepted. See disclaimer below and GNU License 
	!! in gpl-3.0.txt.
	!!
	!! more details in http://kromepackage.org/
	!! also see https://bitbucket.org/krome/krome_stable
	!!
	!!Written and developed by Tommaso Grassi
	!! tommasograssi@gmail.com,
	!! Starplan Center, Copenhagen.
	!! Niels Bohr Institute, Copenhagen.
	!!
	!!Co-developer Stefano Bovino
  	!! sbovino@astro.physik.uni-goettingen.de
	!! Institut fuer Astrophysik, Goettingen.
	!!
	!!Others (alphabetically): D. Galli, F.A. Gianturco, T. Haugboelle, 
	!! J.Prieto, D.R.G. Schleicher, D. Seifried, E. Simoncini, 
	!! E. Tognelli
	!!
	!!
	!!KROME is provided \"as it is\", without any warranty. 
	!! The Authors assume no liability for any damages of any kind 
	!! (direct or indirect damages, contractual or non-contractual 
	!! damages, pecuniary or non-pecuniary damages), directly or 
	!! indirectly derived or arising from the correct or incorrect 
	!! usage of KROME, in any possible environment, or arising from 
	!! the impossibility to use, fully or partially, the software, 
	!! or any bug or malefunction.
	!! Such exclusion of liability expressly includes any damages 
	!! including the loss of data of any kind (including personal data)
	!!*************************************************************\n"""

	if(short): header = """!!*************************************************************
	!!This file has been generated with:
	!!KROME #version# on #date#
	!!see http://kromepackage.org
	!!
	!!Written and developed by Tommaso Grassi
	!!
	!!Co-developer Stefano Bovino
	!!Others (alphabetically): D.Galli, F.A. Gianturco, T. Haugboelle, 
	!! J.Prieto, D.R.G. Schleicher, D. Seifried, E. Simoncini, 
	!! E. Tognelli.
	!!KROME is provided \"as it is\", without any warranty.
	!!*************************************************************\n"""

	datenow = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	header = header.replace("#date#",datenow).replace("#version#",version).replace("#codename#",codename)
	return header.replace("\t","").replace("!!","   ! ")

#################################
#breaks a string (mystr), in piece
# of length (sublen), using a 
# separator (sep)
def trunc(mystr,sublen,sep):
	astr = mystr.split(sep)
	s = z = ""
	for x in astr:
		z += x + sep
		s += x + sep
		if(len(z)>sublen):
			z=""
			s+="\n"
	return s

#################################
#Returns the implicit ode loop
# arguments are number or reactants (nr)
# an products (np)
def get_implicit_ode(nr=3,np=4):
	s = """
n(idx_dummy) = 1.d0
n(idx_g) = 1.d0
n(idx_CR) = 1.d0
"""

	s += "do i=1,nrea\n"

	#r1=arr_r1(i)
	for i in range(nr):
		s += "r"+str(i+1)+" = arr_r"+str(i+1)+"(i)\n"
	#p1=arr_rp(i)
	for i in range(np):
		s += "p"+str(i+1)+" = arr_p"+str(i+1)+"(i)\n"
	s += "rr = k(i)*"+("*".join(["n(r"+str(i+1)+")" for i in range(nr)]))+"\n"

	#dn(r1)=dn(r1)-rr
	for i in range(nr):
		s += "dn(r"+str(i+1)+") = dn(r"+str(i+1)+") - rr\n"
	#dn(p1)=dn(p1)+rr
	for i in range(np):
		s += "dn(p"+str(i+1)+") = dn(p"+str(i+1)+") + rr\n"

	s += "end do\n"
	return s

###########################
#this function returns if the string line
# starts with one of the items in the array 
# of string aarg
def lbeg(aarg,line):
	for arg in aarg:
		if(line[:len(arg)]==arg): return True
	return False

###########################
#this function returns if the string line
# ends with one of the items in the array 
# of string aarg
def lend(aarg,line):
	for arg in aarg:
		if(line[len(line)-len(arg):]==arg): return True
	return False

###############################
#reduce the length of the lines
def cutlines(all_row):
	#check line length
	maxlen = 80
	arowl = []
	for line in all_row:
		if(len(line.split("!")[0])>maxlen):
			erow = line.strip().split(",") #explode using commma
			sall = "" #string
			subrow1 = [] #first line
			subrow2 = [] #second line
			#loop over comma separated parts
			for i in range(len(erow)):
				sall += erow[i]+"," #concatenate string
				#append to the first or the second line depending on the length
				if(len(sall)>maxlen-4):
					subrow2.append(erow[i])
				else:
					subrow1.append(erow[i])

			#join first and second line
			if(subrow1.join().strip()!=""):
				arowl.append((",".join(subrow1)).strip()+", &\n")
			if(subrow2.join().strip()!=""):
				arowl.append((",".join(subrow2))+"\n")
		else:
			arowl.append(line) #append lines of regular length
	return arowl


###############################
#this function indent f90 file and remove multiple blank lines
def indentF90(filename):
	import os
	#check if the file exists else return
	if(not(os.path.isfile(filename))): return
	
	#open file for indent
	fh = open(filename,"rb")
	arow = [] #array of the lines of the indented file
	is_blank = is_amper = False #flags
	nind = 0 #number indent level
	nspace = 2 #number of space for indent
	tokenclose = ["end do","end if","end function","end subroutine","else if","elseif","else","enddo","end module","endif"]
	tokenclose += ["contains","endfunction","endsubroutine","endmodule","end program", "endprogram"]
	tokenopen = ["do ","function","subroutine","contains","else","else if","elseif","module","program"]
	module_head = "!############### MODULE ##############" #module header comment
	module_head_found = False
	for row in fh:
		srow = row.strip() #trim the row
		if(module_head in srow): module_head_found = True #do not duplicate module header
		#check module begin
		if(lbeg(["module"], srow) and not(module_head_found)): 
			arow.append("\n") #blank line
			arow.append(module_head+"\n") #comment
		if(lbeg(tokenclose, srow)): nind -= 1 #check if the line ends with one of tokenclose
		indent = (" "*(nind*nspace)) #compute number of spaces for indent
		if(is_amper): indent = (" "*(2*nspace)) + indent #increas indent in case of previous &
		if(srow!=""):
			if(srow[0]=="#"): indent = "" #no indent for pragmas
		if(not(srow=="" and is_blank)): arow.append(indent+srow+"\n") #append indented line to array of rows
		is_amper = False #is a line after ampersend flag
		if(lend(["&"], srow)): is_amper = True #check if the line ends with &
		is_blank = (srow=="") #flag for blank line mode
		if(lbeg(tokenopen, srow)): nind += 1 #check if the line ends with one of tokenclose
		if(lbeg(["if"],srow) and "then" in srow): nind += 1 #check if line stats with if and has then
	fh.close()

	#arowl = []
	#for j in range(50):
	#	arowl = cutlines(arow)
	#	if(len(arow)==len(arowl)): break
	#	arow = arowl[:]

	arowl = arow[:]

	#write the new file
	fh = open(filename,"w")
	for x in arowl:
		fh.write(x)
	fh.close()

################################
#This function writes an error 
# and exit
def die(msg):
	import sys
	print msg
	sys.exit()

#################################
#This function returns a random quotation properly formtatted
# if qall=True print all the quotes
def get_quote(qall=False):
	import random
	quotes = [["If you lie to the computer, it will get you.","Perry Farrar"],
	["Premature optimization is the root of all evil.","Donald Knuth"],
	["Computers are good at following instructions, but not at reading your mind.","Donald Knuth"],
	["Computer Science is embarrassed by the computer.","Alan Perlis"],
	["Prolonged contact with the computer turns mathematicians into clerks and vice versa.","Alan Perlis"],
	["There are two ways to write error-free programs; only the third one works.","Alan Perlis"],
	["Software and cathedrals are much the same - first we build them, then we pray.","Sam Redwine"],
	["Estimate always goes wrong.","Sumit Agrawal"],
	["Weinberg's Second Law: If builders built buildings the way programmers wrote programs, then the first woodpecker that came"\
	 +" along would destroy civilization.","Gerald Weinberg"],
	["Any sufficiently advanced magic is indistinguishable from a rigged demonstration.",""],
	["Any given program, when running, is obsolete.",""],
	["Programming would be so much easier without all the users.",""],
	["Your Zip file is open.",""],
	["Testing can only prove the presence of bugs, not their absence.","Edsger W. Dijkstra"],
	["If debugging is the process of removing bugs, then programming must be the process of putting them in.","Edsger W. Dijkstra"],
	["God is Real, unless declared Integer.","J. Allan Toogood"],
	["Curiously enough, the only thing that went through the mind of the bowl of petunias as it fell was Oh no, not again.","The"\
	 +" Hitchhiker's Guide to the Galaxy"],
	["Computer science differs from physics in that it is not actually a science.","Richard Feynman"],
	["The purpose of computing is insight, not numbers.","Richard Hamming"],
	["Computer science is neither mathematics nor electrical engineering.","Alan Perlis"],
	["I can't be as confident about computer science as I can about biology. Biology easily has 500 years of exciting problems to work"\
	 +" on. It's at that level.","Donald Knuth"],
	["The only legitimate use of a computer is to play games.","Eugene Jarvis"],
	["UNIX is user-friendly, it just chooses its friends.","Andreas Bogk"],
	["Quantum mechanic Seth Lloyd says the universe is one giant, hackable computer. Let's hope it's not running Windows.","Kevin Kelly"],
	["Computers are useless. They can only give you answers.","Pablo Picasso"],
	["Computers in the future may weigh no more than 1.5 tons.","Popular Mechanics (1949)"],
	["Don't trust a computer you can't throw out a window.","Steve Wozniak"],
	["Computers are like bikinis. They save people a lot of guesswork.","Sam Ewing"],
	["If the automobile had followed the same development cycle as the computer, a Rolls-Royce would today cost $100, get a million"\
	 +" miles per gallon, and explode once a year, killing everyone inside.","Robert X. Cringely"],
	["Computers are getting smarter all the time. Scientists tell us that soon they will be able to talk to us.  (And by 'they',"\
	 " I mean 'computers'.  I doubt scientists will ever be able to talk to us.)","Dave Barry"],
	["Most software today is very much like an Egyptian pyramid with millions of bricks piled on top of each other, with no structural\
	 integrity, but just done by brute force and thousands of slaves.","Alan Kay"],
	["No matter how slick the demo is in rehearsal, when you do it in front of a live audience, the probability of a flawless "\
	 + "presentation is inversely proportional to the number of people watching, raised to the power of the amount of money involved.",\
	"Mark Gibbs"],
	["Controlling complexity is the essence of computer programming.","Brian Kernigan"],
	["Software suppliers are trying to make their software packages more 'user-friendly'...  Their best approach so far has been to take"\
	 +" all the old brochures and stamp the words 'user-friendly' on the cover.","Bill Gates"],
	["Programmers are in a race with the Universe to create bigger and better idiot-proof programs, while the Universe is trying to"\
	 + " create bigger and better idiots. So far the Universe is winning.","Rich Cook"],
	["To iterate is human, to recurse divine.","L. Peter Deutsch"],
	["Should array indices start at 0 or 1?  My compromise of 0.5 was rejected without, I thought, proper consideration.","Stan Kelly-Bootle"],
	["Any code of your own that you haven't looked at for six or more months might as well have been written by someone else.","Eagleson's Law"],
	["All science is either physics or stamp collecting.", "Ernest Rutherford"],
	["Done is better than perfect.", ""],
	["Computers are like Old Testament gods; lots of rules and no mercy","Joseph Campbell"],
	["A computer lets you make more mistakes faster than any other invention with the possible exceptions of handguns and Tequila.",\
	 "Mitch Ratcliffe"],
	["Computer Science is no more about computers than astronomy is about telescopes.","Edsger W. Dijkstra"],
	["To err is human, but to really foul things up you need a computer.","Paul Ehrlich"],
	["Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are,"\
	 +" by definition, not smart enough to debug it.","Brian W. Kernighan"],
	["Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live.","Martin Golding"],
	["One of my most productive days was throwing away 1000 lines of code.","Ken Thompson "],
	["And God said, \"Let there be light\" and segmentation fault (core dumped)",""],
	["Today, most software exists, not to solve a problem, but to interface with other software","Ian Angell"],
	["Measure twice, cut once",""],
	["Weeks of programming can save you hours of planning",""],
	["All models are wrong; some models are useful","George Box"],
	["The generation of random numbers is too important to be left to chance","Robert Coveyou"],
	["Problems worthy / of attack / prove their worth / by hitting back","Piet Hein"],
	["Good, Fast, Cheap: Pick any two","Memorandum RFC 1925"],
	["One size never fits all","Memorandum RFC 1925"],
	["No matter how hard you push and no matter what the priority,you can't increase the speed of light","Memorandum RFC 1925"],
	["Chemistry has been termed by the physicist as the messy part of physics", "Frederick Soddy "],
	["Don't worry if it doesn't work right. If everything did, you'd be out of a job.", "Mosher's Law"],
	["Beware of bugs in the above code; I have only proved it correct, not tried it.", "Donald Knuth"],
	["Given enough eyeballs, all bugs are shallow.", "Eric S. Raymond"]
	]
	qrange = 1
	print 
	if(qall): qrange = len(quotes)
	for i in range(qrange):
		irand = int(random.random()*(len(quotes)))
		if(qall): irand = i
		qtup = quotes[irand]
		myqt = trunc(str(irand+1)+". "+qtup[0],40," ").upper().strip()
		amyqt = myqt.split("\n")
		lqt = max([len(x) for x in amyqt])
		print
		if(i==0): print "*"*lqt
		print myqt
		if(qtup[1].strip()==""): qtup[1] = "Anonymous"
		print "--- "+qtup[1]
		if(i==qrange-1): 
			print "*"*lqt
			print

